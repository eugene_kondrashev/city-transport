#! /usr/bin/env python

import os.path
import sys, time
import re
import json
import logging
import requests
LOG_FILENAME = '/tmp/dumper_daemon.log'
logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG,)


from daemon import Daemon
from django.contrib.gis.geos import Point
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DBENGINE = 'django.contrib.gis.db.backends.spatialite'
DBNAME = os.path.join(BASE_DIR, 'db.sqlite3')

def init_django(is_syncdb=False):

    sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

    from django.conf import settings

    if not settings.configured:
        settings.configure(
            DATABASES={
               'default': {
                   'ENGINE': DBENGINE,
                   'NAME': DBNAME,

                }
            },
            INSTALLED_APPS=("tram_groundhog_day", ),
        )

    if is_syncdb:
        logging.debug("Create the tables from the models")
        __syncdb()

def __syncdb():
    if DBENGINE == 'django.contrib.gis.db.backends.spatialite':
        from subprocess import call
        call(["rm", DBNAME])
        # Seems like we cannot use spatialite oob, first there is need to call InitSpatialMetaData and then add some ugly srs definition
        # see https://docs.djangoproject.com/en/dev/ref/contrib/gis/install/spatialite
        # see http://gis.stackexchange.com/questions/40471/why-does-addgeometrycolumn-error-using-the-libspatialite-2-dll-with-pysqlite2
        call(["spatialite", DBNAME, '''SELECT InitSpatialMetaData(); INSERT INTO spatial_ref_sys (srid, auth_name, auth_srid, ref_sys_name, proj4text) VALUES (4326, 'epsg', 4326, 'WGS 84', '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs')'''])
    from django.core.management import call_command
    call_command('syncdb')


def parse_event_data(data):
    from models import EventData
    c = u'c' in data and int(data[u'c'])
    x, y = data[u'x'], data[u'y']
    x2, y2 = data[u'x2'], data[u'y2']
    z = int(data[u'z'])
    t = long(data[u't'])
    f = int(data[u'f'])
    i = int(data[u'i'])
    sc = int(data[u'sc'])
    s = int(data[u's'])
    tp = data[u'tp']
    l = data[u'l']
    p = Point(x, y)
    p2 = Point(x2, y2)
    ed = EventData(c=c, f=f, i=i, tp=tp, sc=sc, s=s, t=t, l=l, z=z, p=p, p2=p2)
    return ed

def parse_event(event):
    from models import Event
    i = int(event[u'i'])
    t = event[u't']
    data = event[u'd']
    event_data = parse_event_data(data)
    return event_data, Event(i=i, t=t, d=event_data)

def get_events(address, eid, session, cookies):
    payload = {'sid': eid, }
    event_response = session.post('%s/avl_events' % address, data=payload, cookies=cookies).content
    event_response_json = json.loads(event_response[2:])
    return event_response_json[u'events']

def process_events(address, eid, session, cookies):
    events = get_events(address, eid, session, cookies)
    for e in events:
        try:
            logging.debug('Current event: %s' % e)
            if not u'cmds' in e[u'd']:
                event_data, event = parse_event(e)
                event_data.save()
                event.d = event_data
                event.save()
            else:
                logging.debug('Skipping an event since cmds as a data returned')
        except:
            logging.exception('Got an exception during processing of an event')


class Impl(Daemon):
    def __init__(self, pidfile, address=None, session=None, cookies=None, eid=None, stdin='/dev/null', stdout='/dev/null', stderr='/dev/null'):
        self.address = address
        self.session = session
        self.cookies = cookies
        self.eid = eid
        Daemon.__init__(self, pidfile, stdin, stdout, stderr)

    def run(self):
        while True:
            try:
                process_events(self.address, self.eid, self.session, self.cookies)
            except:
                logging.exception('Got an exception during processing events')
            time.sleep(5)

def process_initial_data(initial_data_json):
    from models import Event
    for item in initial_data_json[u'items']:
        try:
            logging.debug('Current item: %s' % item)
            pos = item[u'pos']
            event_data = parse_event_data(pos)
            id_ = item[u'id']
            t = None
            event_data.save()
            event = Event(i=id_, t=t, d=event_data)
            event.save()
        except:
            logging.exception('Got an exception during processing of an item')

def login(address, username, password):
    headers = {'User-Agent': 'Mozilla/5.0'}
    payload = {'user':username,'passw':password,'lang':'english', 'action':'login', 'submit':'Enter'}
    link    = '%s/login_action.html' % address
    session = requests.Session()
    session.get(link,headers=headers)
    cookies = requests.utils.cookiejar_from_dict(requests.utils.dict_from_cookiejar(session.cookies))
    session.post(link,headers=headers,data=payload, cookies=cookies)

    response_content = session.get('%s/' % address).content
    logging.debug('*' * 50)

    p = 'Wialon\.init_state_impl\((\{.*?\})\);'
    m = re.search(p, response_content, re.DOTALL)
    if m:
        wialon_init_data = m.group(1)
        wialon_init_json = json.loads(wialon_init_data)

        return wialon_init_json, session, cookies
    raise ValueError('Invalid response received')

if __name__ == "__main__":

    daemon = Impl('/tmp/tgd_dumper_daemon.pid')
    if len(sys.argv) > 1:
        if 'start' == sys.argv[1]:
            if len(sys.argv) > 4:
                address = sys.argv[2]
                username = sys.argv[3]
                passwd = sys.argv[4]
                is_syncdb = len(sys.argv) > 5 and 'syncdb' == sys.argv[5]
                init_django(is_syncdb=is_syncdb)
                initial_data, session, cookies = login(address, username, passwd)
                process_initial_data(initial_data)
                eid = initial_data['eid']
                daemon = Impl(daemon.pidfile, address=address, session=session, cookies=cookies, eid=eid)
                daemon.start()
            else:
                print 'No address username password provided'
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        else:
            print "Unknown command"
            sys.exit(2)
            sys.exit(0)
    else:
        print "usage: %s start <address> <username> <password> [syncdb]|stop|restart" % sys.argv[0]
        sys.exit(2)
