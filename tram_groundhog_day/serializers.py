# coding=utf-8
'''
Created on 07 Aug 2014

@author: ekondrashev
'''
from rest_framework import serializers
from models import EventData


class EventData(serializers.ModelSerializer):
    class Meta:
        model = EventData
        fields = ('c', 'f', 'i', 'tp', 'sc', 's', 't', 'l', 'z', 'p', 'p2', 'date')
