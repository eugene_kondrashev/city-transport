# coding=utf-8
'''
Created on Aug 22, 2014

@author: ekondrashev
'''
from django.db import models as m
from django.contrib.gis.db import models


class EventData(models.Model):
    c = models.IntegerField(null=True)
    f = models.IntegerField()
    i = models.IntegerField()
    tp = models.CharField(max_length=10)
    sc = models.IntegerField()
    s = models.IntegerField()
    t = models.BigIntegerField()
    l = models.TextField()
    z = models.IntegerField()

    p = models.PointField()
    p2 = models.PointField()
    date = models.DateTimeField(auto_now_add=True, blank=False)
    objects = models.GeoManager()
    class Meta:
        app_label = 'tram_groundhog_day'


class Event(models.Model):
    i = models.IntegerField()
    t = models.CharField(max_length=15, null=True)
    d = models.ForeignKey(EventData)

    class Meta:
        app_label = 'tram_groundhog_day'
