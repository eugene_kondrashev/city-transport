#! /usr/bin/env python

import os.path
import sys, time
import re
import json
import logging
import requests
import time
import datetime as dt
import pika

LOG_FILENAME = '/tmp/emulator_daemon.log'
logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG,)


from daemon import Daemon
from django.contrib.gis.geos import Point
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DBENGINE = 'django.contrib.gis.db.backends.spatialite'
DBNAME = os.path.join(BASE_DIR, 'dbdump.sqlite3')


def init_django():

    sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

    from django.conf import settings

    if not settings.configured:
        settings.configure(
            DATABASES={
               'default': {
                   'ENGINE': DBENGINE,
                   'NAME': DBNAME,

                }
            },
            INSTALLED_APPS=("tram_groundhog_day", ),
        )


class Impl(Daemon):
    def __init__(self, pidfile, mq_connection=None, queue_name=None, stdin='/dev/null', stdout='/dev/null', stderr='/dev/null'):
        self.connection = mq_connection
        self.queue_name = queue_name
        if self.connection:
            self.channel = self.connection.channel()
            self.channel.queue_declare(queue=queue_name)
        Daemon.__init__(self, pidfile, stdin, stdout, stderr)

    def get_next_date(self, now, now_seconds):

        delta_days = (now_seconds - MIN_DATE_SECONDS)//86400
        delta = dt.timedelta(days = delta_days)

        next_date = now - delta
        return next_date

    def run(self):
        from serializers import EventData as serializer
        while True:
            start = dt.datetime.now()
            start_seconds = unix_time(start)
            try:
                next_date = self.get_next_date(start, start_seconds)
                result = fetch_eventdata_range(next_date)
                logging.debug(len(result))
                if result:
                    json_ = json.dumps(serializer(result, many=True).data, default=date_handler)
                    logging.debug(json_)
                    self.channel.basic_publish(exchange='',
                              routing_key=self.queue_name,
                              body=json_)
            except:
                logging.exception('Got an exception during fetching events')
            end_seconds = unix_time(dt.datetime.now())
            sleeptime = 5 - (end_seconds - start_seconds)
            logging.debug('sleeptime: %d' % sleeptime)
            if sleeptime > 0:
                time.sleep(sleeptime)

    def atexit(self):
        try:
            logging.debug('Closing mq connection')
            self.connection.close()
            logging.debug('Connection closed')
        except:
            logging.exception('Error closing connection')
        Daemon.atexit()

def date_handler(obj):
    return js_timestamp(obj) if isinstance(obj, dt.datetime) else obj


def js_timestamp(dt):
    """ Return a javascript timestamp from a datetime object.

        Javascript times are milliseconds since the epoch,
        whereas python uses just seconds, so lets return the
        times in the javascript format, so it's unambiguous to
        jqPlot.

    """
    return unix_time(dt) * 1000

def unix_time(dt):
    return time.mktime(dt.timetuple())


def fetch_eventdata_range(curr_date):
    from models import EventData
    second = dt.timedelta(seconds=1)

    initial_date = dt.datetime.fromtimestamp(unix_time(curr_date))

    start_date = initial_date
    end_date = start_date + second
    logging.debug( (start_date, end_date))
    eventdatas = EventData.objects.filter(date__range=(start_date, end_date))

    result = []
    while eventdatas:
        result.extend(eventdatas)
        start_date = end_date
        end_date = start_date + second
        eventdatas = EventData.objects.filter(date__range=(start_date, end_date))

    end_date = initial_date
    start_date = end_date - second
    eventdatas = EventData.objects.filter(date__range=(start_date, end_date))
    while eventdatas:
        result.extend(eventdatas)
        start_date = end_date
        end_date = start_date + second
        eventdatas = EventData.objects.filter(date__range=(start_date, end_date))
    return result

MIN_DATE_SECONDS = 1409665964
QUEUE_NAME = 'raw_event'
PID_FILE = '/tmp/tgd_emulator_daemon.pid'

if __name__ == "__main__":

    if len(sys.argv) > 1:
        daemon = Impl(PID_FILE)
        if 'start' == sys.argv[1]:
            connection = pika.BlockingConnection(pika.ConnectionParameters(
                    host='localhost'))
            logging.debug(connection)
            daemon = Impl(PID_FILE, mq_connection=connection, queue_name=QUEUE_NAME)
            init_django()
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        else:
            print "Unknown command"
            sys.exit(2)
            sys.exit(0)
    else:
        print "usage: %s start <address> <username> <password> [syncdb]|stop|restart" % sys.argv[0]
        sys.exit(2)
