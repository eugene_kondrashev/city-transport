# coding=utf-8
'''
Created on Aug 21, 2014

@author: ekondrashev
'''

import sys
from os import path
import collections
import datetime
import os.path
from django.contrib.gis.geos import Point

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

sys.path.insert(0, path.abspath(path.join(path.dirname(__file__), '..')))

from django.core.management import call_command
from django.conf import settings

if not settings.configured:
    settings.configure(
        DATABASES={
           'default': {
               'ENGINE': 'django.db.backends.sqlite3',
               'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),

            }
        },
        INSTALLED_APPS=("tram_groundhog_day", )
    )

print ("Create the tables from the models")
call_command('syncdb')

from models import *

# ed = EventData(c = 123,
#     f = 234,
#     i = 345,
#     tp = 'udp',
#     sc = 567,
#     s = 678,
#     t = 879,
#     l = 'asdf',
#     z = -1
#     )
#
# ed.save()

import requests
import re
import json

# headers = {'User-Agent': 'Mozilla/5.0'}
# payload = {'user':'Херсонский сквер','passw':'спанчбоб'}
#
# session = requests.Session()
# print session.post('http://91.197.49.170:8022/login_action.html',headers=headers,data=payload).content

headers = {'User-Agent': 'Mozilla/5.0'}
payload = {'user':'Херсонский сквер','passw':'спанчбоб','lang':'english', 'action':'login', 'submit':'Enter'}
link    = 'http://91.197.49.170:8022/login_action.html'
session = requests.Session()
resp    = session.get(link,headers=headers)
# did this for first to get the cookies from the page, stored them with next line:
cookies = requests.utils.cookiejar_from_dict(requests.utils.dict_from_cookiejar(session.cookies))
resp    = session.post(link,headers=headers,data=payload, cookies=cookies)
#used firebug to check POST data, password, was actually 'pass', under 'net' in param.
#and to move forward from here after is:
response_content = session.get('http://91.197.49.170:8022/').content
print '*' * 50

p = 'Wialon\.init_state_impl\((\{.*?\})\);'
m = re.search(p, response_content, re.DOTALL)
if m:
    wialon_init_data = m.group(1)
    wialon_init_json = json.loads(wialon_init_data)
    print wialon_init_json
#     eid = wialon_init_json['eid']
#     payload = {'sid': eid, }
#     event_response = session.post('http://91.197.49.170:8022/avl_events', data=payload, cookies=cookies).content
#     event_response_json = json.loads(event_response[2:])
#     for event in event_response_json[u'events']:
#         print event
#         try:
#             event_data = parse_event_data(event)
#         except:
#             pass
#         data = event[u'd']
#         c = int(data[u'c'])
#         x, y = data[u'x'], data[u'y']
#         x2, y2 = data[u'x2'], data[u'y2']
#         z = data[u'z']
#         t = data[u't']
#         print 'x=%s, y=%s' % (x, y)
#         print 'x2=%s, y2=%s' % (x2, y2)
#         print 'z=%s, t=%s' % (z, t)
#         ed = EventData(c = 123, f = 234, i = 345,
#         tp = 'udp',
#         sc = 567,
#         s = 678,
#         t = 879,
#         l = 'asdf',
#         z = -1
#         )
#
#         ed.save()
#
# else:
#     print 'No eid found'
