#! /usr/bin/env python
import os.path
import sys, time
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DBENGINE = 'django.contrib.gis.db.backends.spatialite'
DBNAME = r'/home/ekondrashev/sources/city-transport/dbdump.sqlite3'

def init_django():

    sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

    from django.conf import settings

    if not settings.configured:
        settings.configure(
            DATABASES={
               'default': {
                   'ENGINE': DBENGINE,
                   'NAME': DBNAME,

                }
            },
            INSTALLED_APPS=("tram_groundhog_day", ),
        )

if __name__ == "__main__":
    init_django()
    from tram_groundhog_day.models import EventData
    from tram_groundhog_day.models import Event
    print Event.objects.filter(i=5368)[0].d.p
#     print EventData.objects.all()[0].p
#     print EventData.objects.filter(i=)[0].p
